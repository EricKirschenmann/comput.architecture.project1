/**
 * @author Eric Kirschenmann & Kenny Palumbo
 * @Date: September 22, 2014
 * 
 */
package endian;

//import the built in scanner for taking in user data
import java.util.Scanner;

public class BigEndian {
	
	//variables that control the
	//current row and column of the array
	public static int row = 0;
	public static int col = 0;
	
	//main method of the program
	@SuppressWarnings("resource")
	public static void main(String [] args) {
		
		//an array of bytes
		//currently small to allow faster testing
		byte [][] end = new byte [20][4];

		//scanner used to take user input
		Scanner input = new Scanner(System.in);
		
		//string that collects the user's input for string or integer or quit
		String ans = "";
		boolean rowCheck = false; //boolean that checks the user's response to choosing a new row
		
		//scanner for choosing a row
		Scanner choose = new Scanner(System.in);
		
		//never ending loop that runs the main program
		//looping until the user quits
		while(true) {
			
			//prompt the user whether they would like to enter a string or an integer
			//as well as giving the option to quit using "Q" or "q"
			System.out.println("Would you like to enter a String or an Integer? S or I:"
					+ "\nQ to quit or P to print your current data. ");
			ans = input.nextLine(); //receive the user's input
			
			//Check to see if the user wants to enter a string, ignoring case
			if(ans.equals("S") || ans.equals("s")) {
				
				//prompt user to see if they want to enter a row for their data
				System.out.println("Do you want to choose the row to store your data? Y or N");
				String chooseR = input.nextLine(); //store their answer into a string
				
				//check the user's answer 
				if(chooseR.equals("Y") || chooseR.equals("y")) {
					//if they say yes prompt them for their row
					System.out.println("Please enter your row: ");
					row = choose.nextInt(); //store the row number
					rowCheck = true; //set boolean to true
					col = 0; //reset column location to the beginning
				}
				else if(chooseR.equals("N") || chooseR.equals("n")) {
					//if the user says no set the boolean to false
					rowCheck = false;
				}
				
				//create a string that stores the user's string input
				String user;
				
				//if the user did not choose a row auto-set row
				if(!rowCheck)
					newLoc();
				
				//prompt the user for their string
				System.out.println("Please enter your string: ");
				//collect the user's string
				user = input.nextLine();
				
				//if the user did say yes
				if(rowCheck) {
					//clear out the old data
					if(user.length() > 4) {
						for(int x = 0; x < 4; x++) {
							for(int y = row; y <= row + 1; y++) {
								end[y][x] = 0;
							}
						}
					}
					//clear out old data
					else if(user.length() <= 4) {
						for(int x = 0; x < 4; x++) {
							end[row][x] = 0;
						}
					}
				}
				
				//loop that stores the user's string into the
				//array of bytes
				if(row < 14) {
					for(int z = 0; z < user.length(); z++) {
							//check to make sure that the array does not go out of bounds
							if(col < end[row].length) {
								//convert the char to a byte and stores the data
								end[row][col] = (byte)(user.charAt(z));
								//increment the column that the data is being stored into
								col++;
								
								//temp = createHeader(1, row);
								setHeader(row, 1, end);
							}
							//if the storage hits the end of the row
							else {
								//increment the row
								row++;
								//check to make sure there is still space in the array
								if(row == end.length) {
									//give an error if there is no space then break the loop
									System.err.print("\nSorry out of space!");
									break;
								}
								//reset to the first column of the row
								col = 0;
								//store the data into the array
								end[row][col] = (byte)(user.charAt(z));
								//increment the column
								col++;
								
								//temp = createHeader(1, row);
								setHeader(row, 1, end);
							}
					}
				}
				else if(row == 14 && (user.length() <= 4)) {
					for(int z = 0; z < user.length(); z++) {
							//check to make sure that the array does not go out of bounds
							if(col < end[row].length) {
								//convert the char to a byte and stores the data
								end[row][col] = (byte)(user.charAt(z));
								//increment the column that the data is being stored into
								col++;
								
								//temp = createHeader(1, row);
								setHeader(row, 1, end);
							}
							//if the storage hits the end of the row
							else {
								//increment the row
								row++;
								//check to make sure there is still space in the array
								if(row == end.length) {
									//give an error if there is no space then break the loop
									System.err.print("\nSorry out of space!");
									break;
								}
								//reset to the first column of the row
								col = 0;
								//store the data into the array
								end[row][col] = (byte)(user.charAt(z));
								//increment the column
								col++;
								
								//create then set the header file
								//temp = createHeader(1, row);
								setHeader(row, 1, end);
							}
					}
				}
				//if the user enters a string that is longer than 4 characters while writing to
				//the last row of user data. Give the user the error and instruct them on what to do next.
				else if(row == 14 && (user.length() > 4)) {
					System.out.println("Sorry! You're at the end and your string"
							+ "\ncannot be more than 4 characters. "
							+ "\nPlease re-enter or choose a new row.");
					
				}
				//anything that doesn't 
				else {
					//display an error
					System.err.println("Error, somethings wrong");
				}
				
			}
			//if the user chooses to enter an integer
			else if(ans.equals("I") || ans.equals("i")) {
				
				//prompt user to see if they want to enter a row for their data
				System.out.println("Do you want to choose the row to store your data? Y or N");
				String chooseR = input.nextLine(); //store their answer into a string
				
				//check the user's answer 
				if(chooseR.equals("Y") || chooseR.equals("y")) {
					//if they say yes prompt them for their row
					System.out.println("Please enter your row: ");
					row = choose.nextInt(); //store the row number
					rowCheck = true; //set boolean to true
					col = 0; //reset column location to the beginning
				}
				else if(chooseR.equals("N") || chooseR.equals("n")) {
					//if the user says no set the boolean to false
					rowCheck = false;
				}
				
				//temporary array of bytes for the conversion
				byte [] convert;
				
				//scanner and integer used to take user's integer input
				Scanner integers = new Scanner(System.in);
				int data = 0;
				
				//change the location in the array if the user
				//doesn't specify
				if(!rowCheck)
					newLoc();
				//clear out the row of old data if they did
				if(rowCheck) {
					for(int x = 0; x < 4; x++) {
						end[row][x] = 0;
					}
				}
				
				//prompt the user for their integer and take the input
				System.out.println("Please enter your integer: ");
				data = integers.nextInt();
				
				//convert the 32bit integer to bytes
				convert = intToByteArray(data);
				
				//store the converted bytes into the row of the array
				for(int col = 0; col < convert.length; col++) {
					end[row][col] = convert[col]; 
				}
				
				//temp = createHeader(-1, row);
				setHeader(row, -1, end);
				
				//increment the row
				row++;
				
			}
			//if the user chooses to quit
			else if(ans.equals("Q") || ans.equals("q")) {
				//thank the user
				System.out.println("Thank you.");
				//exit the program
				break;
			}
			//print the current array
			else if(ans.equals("P") || ans.equals("p"))
				printEnd(end);
			//if input is unrecognized
			else {
				//tell the user that the entry wasn't recognized
				System.err.println("Sorry! Command not recognized");
			}
			
		}
		
		//create littleEndian object and sends the
		//end array into the constructor
		@SuppressWarnings("unused")
		littleEndian little = new littleEndian(end);
		
		
	}
	
	//method that prints out the array
	public static void printEnd(byte [][] end) {
		//nested for loops that go through each of the
		//elements in the array printing them out.
		for(int x = 0; x < 15; x++) {
			for(int y = 0; y < 4; y++) {
				System.out.print(end[x][y] + "\t");
			}
			System.out.println();
		}
	}
	
	/**
	 * Method that uses bitwise shifts to convert 32bit integers
	 * to an array of bytes
	 * @author http://stackoverflow.com/a/6515001
	 * @param value
	 * @return
	 */
	public static byte[] intToByteArray(int value) {
	    return new byte[] {
	            (byte)(value >>> 24),
	            (byte)(value >>> 16),
	            (byte)(value >>> 8),
	            (byte)value};
	}
	
	//converts and array of bytes into a long
	//works like magic, but isn't used other than for testing purposes.
	public static long getUInt32(byte[] bytes){
	    long value = bytes[3] & 0xFF;
	    value |= (bytes[2] << 8) & 0xFFFF;
	    value |= (bytes[1] << 16) & 0xFFFFFF;
	    value |= (bytes[0] << 24) & 0xFFFFFFFF;
	    return value;
	}																																									

	//method that moves the input to the next row
	public static void newLoc() {
		
		//check if the input is at the start of a new row or not,
		//if it isn't reset to the first column of the next row
		if(col != 0) {
			row++;
			col = 0;
		}

	}
	
	//method that sets the headers based on data type in each row
	public static void setHeader(int row, int check, byte[][] end) {
		
		//set the header
		byte header = (byte)check;
		
		//Most beautiful switch statement ever created to set the header
		//files into the array.
		switch (row) {
		
		case 0:
			end[15][0] = header;
			break;
		case 1:
			end[15][1] = header;
			break;
		case 2:
			end[15][2] = header;
			break;
		case 3:
			end[15][3] = header;
			break;
		case 4:
			end[16][0] = header;
			break;
		case 5:
			end[16][1] = header;
			break;
		case 6:
			end[16][2] = header;
			break;
		case 7:
			end[16][3] = header;
			break;
		case 8:
			end[17][0] = header;
			break;
		case 9:
			end[17][1] = header;
			break;
		case 10:
			end[17][2] = header;
			break;
		case 11:
			end[17][3] = header;
			break;
		case 12:
			end[18][0] = header;
			break;
		case 13:
			end[18][1] = header;
			break;
		case 14:
			end[18][2] = header;
			break;
		}
		
		
	}
	
	
}

