package endian;
//test
public class littleEndian {
	
	//create two arrays that are used for storing the header data
	//and the little Endian array
	private static byte [][] lilEnd = new byte [15][4];
	private static byte [] head = new byte [20];

	//constructor method
	public littleEndian(byte [][] end) {
		//call the convert method to begin the process
		//of changing Big Endian to little Endian
		convert(end);
	}
	
	//method that transfers the header data
	private static void setData(byte [][] end) {
		
		//transfer header data to an array
		//that is easier to work with
		head[0]  = end[15][0];
		head[1]  = end[15][1];
		head[2]  = end[15][2];
		head[3]  = end[15][3];
		head[4]  = end[16][0];
		head[5]  = end[16][1];
		head[6]  = end[16][2];
		head[7]  = end[16][3];
		head[8]  = end[17][0];
		head[9]  = end[17][1];
		head[10] = end[17][2];
		head[11] = end[17][3];
		head[12] = end[18][0];
		head[13] = end[18][1];
		head[14] = end[18][2];
		head[15] = end[18][3];
		head[16] = end[19][0];
		head[17] = end[19][1];
		head[18] = end[19][2];
		head[19] = end[19][3];
		
	}
	
	//method that converts into little endian
	private static void convert(byte [][] end) {

		//calls the method to store header data
		setData(end);

		//copy data into little endian array
		for(int x = 0; x <= 14; x++) {
			//if the data is a string store it in reverse order
			if(head[x] == 1) {
				lilEnd[x][0] = end[x][3];
				lilEnd[x][1] = end[x][2];
				lilEnd[x][2] = end[x][1];
				lilEnd[x][3] = end[x][0];
			}
			//if the data is an integer store in the same order
			else if(head[x] == -1) {
				for(int y = 0; y <= 3; y++) {
					lilEnd[x][y] = end[x][y];
				}
			}
		}
		
		//call the method to display the array
		display(end);
		
	}
	
	//method that converts bytes back into chars
	public static void toChar(byte [] temp, char [] ascii) {
		
		//store the chars that are converted into a temporary array
		//used for displaying the characters
		for(int x = 0; x < 4; x++) {
			//replace 0's in the array with spaces
			if(temp[x] == 0)
				ascii[x] = ' ';
			else
				//cast the byte data into a char
				ascii[x] = (char)(temp[x]);
		}
		

	}
	
	//method that prints out the converted data
	public static void display(byte [][] end) {
		
		//create two arrays which are used to temporarily
		//store a row of bytes and chars used for conversion
		byte [] temp = new byte [4];
		char [] ascii = new char [4];
		
		//loop that checks the data in the header file
		//then prints the data stored in the little endian array
		for(int x = 0; x < 15; x++) {
			//if the data in the row is an integer
			if(head[x] == -1) {
				//print out the data
				for(int i = 0; i < 4; i++) {
					System.out.print(lilEnd[x][i] + "\t");
				}
				System.out.println();
			}
			//if the header indicates a string
			else if(head[x] == 1) {
				//store the bytes into the temp array
				for(int i = 0; i < 4; i++) {
					temp[i] = lilEnd[x][i];
				}
				//pass the data in the temp array and the char array
				//to the method that converts the data types
				toChar(temp, ascii);
				//print out the array of chars
				for(int y = 0; y < 4; y++) {
					System.out.print(ascii[y] + "\t");
				}
				System.out.println();
			}
			//default just prints the array if no header data is found
			else {
				for(int y = 0; y < 4; y++) {
					System.out.print(lilEnd[x][y] + "\t");
				}
				System.out.println();
			}
				

		}

	}
	
	//method that prints the untouched arrays
	//not used in the program just for testing purposes.
	public static void printRaw(byte [][] end) {
		//big endian
		System.out.println("Big Endian: ");
		for(int x = 0; x < end.length; x++) {
			for(int y = 0; y < end[x].length; y++) {
				System.out.print(end[x][y] + "\t");
			}
			System.out.println();
		}

		//little endian
		System.out.println("Little Endian: ");
		for(int x = 0; x < lilEnd.length; x++) {
			for(int y = 0; y < lilEnd[x].length; y++) {
				System.out.print(lilEnd[x][y] + "\t");
			}
			System.out.println();
		}
	}
	
}
